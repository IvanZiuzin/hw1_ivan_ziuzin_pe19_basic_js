"use strict"
// Этот код работает в современном режиме

// Пример для теоретического вопроса №3
// alert('5' * '6');
console.log(typeof ('5' * '6'), ('5' * '6'));


// ПРАКТИЧЕСКОЕ ЗАДАНИЕ №1 -----------Task#1-----------

// Переменные:
let name = 'Ivan Ziuzin';
let admin = name;
let homeworkJavaScript = '#1_PE19_BASIC_JS';

name = admin;

// Консоль:
console.log(typeof admin, admin);



// ПРАКТИЧЕСКОЕ ЗАДАНИЕ №2 ------------------------
alert(`Здравствуйте, это домашнее задание ${homeworkJavaScript} Task#2 студента ${name}`);
confirm('Вы готовы ответить на несколько вопросов для Task#2? - нажмите "Да", если готовы - "Нет" - если Вам не интересно');
console.log(typeof confirm, confirm);

// Переменные: - задаём 8 дней 
let days = prompt('Задайте, пожалуйста, для Task#2 количество дней, начните с "8":', 8);
console.log(typeof days, days);

const singleDayHours = 24;
const singleDayMinutes = 1440;
const singleDaySeconds = 86400;


let daysSecondsScore = (days * singleDaySeconds);
// результат при введении количества дней _8_, будет результат: 691200 секунд

// Консоль:
console.log(typeof daysSecondsScore, daysSecondsScore);


// ПРАКТИЧЕСКОЕ ЗАДАНИЕ №3 --------------------------

let singleYearDays = prompt ('Напишите, пожалуйста, для Task#3 количество дней в году - 365:-)', 365);
let singleYearMinutes = (singleYearDays * singleDayMinutes);

// Консоль:
// Выводим на консоль то, что спросили у пользователя:
console.log(typeof singleYearDays, singleYearDays); 
// Выводим на консоль переменную singleYearMinutes:
console.log(typeof singleYearMinutes, singleYearMinutes);
console.log(singleYearMinutes > singleDaySeconds);
console.log('Конец домашнего задания :)');

// Приветствуем пользователя:
alert('Oтветы на теоретические вопросы ДЗ№1 находятся на странице документа. Хорошего, Вам дня:)')